import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {ShopService} from './modules/shop/services/shop.service';
import {ShopModule} from './modules/shop/shop.module';
import {ShopComponent} from './modules/shop/shop.component';

const APP_ROUTES: Routes = [
  {path: '', component: ShopComponent },
  {path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    // ReactiveFormsModule,
    ShopModule,
    RouterModule.forRoot(APP_ROUTES)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
