import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInfoFormComponent } from './add-info-form.component';
import {ReactiveFormsModule} from '@angular/forms';
import {FormatDatePipe} from '../../pipes/format-date.pipe';

describe('AddInfoFormComponent', () => {
  let component: AddInfoFormComponent;
  let fixture: ComponentFixture<AddInfoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ ReactiveFormsModule ],
      declarations: [ AddInfoFormComponent, FormatDatePipe ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInfoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
