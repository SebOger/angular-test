import {Component, Input, OnInit} from '@angular/core';
import {FormGroup} from '@angular/forms';
import {Shop} from '../../models/shop';
import {FormDataService} from '../../services/form-data.service';
import {ShopService} from '../../services/shop.service';
import {TYPE_HOURS} from '../../mocks/type-hours';
import {OnChangeCustom} from '../../customFct/Onchange';
import {AddInfo} from '../../models/add-infos/add-info';

@Component({
  selector: 'app-add-info-form',
  templateUrl: './add-info-form.component.html',
  styleUrls: ['./add-info-form.component.scss']
})
export class AddInfoFormComponent implements OnInit {
  infosForm: FormGroup;
  addInfos: AddInfo;
  typesHours = TYPE_HOURS;
  @OnChangeCustom<string>(function (value, simpleChange) {
    console.log('fddddddddddddddddddddddddddddd');
    console.log(simpleChange);
    if (simpleChange.isFirstChange() !== true) {
      this.OpenOrClose =  !this.OpenOrClose ;
      // this.changeTypeHourForm();
      alert(`title is changed to: ${value}`);
    }
  })
  private _selectTypeHour: string;
  OpenOrClose: boolean ;

  // get OpenOrClose(): boolean {
  //   return this._OpenOrClose;
  //
  // }
  // set OpenOrClose(value: boolean ) {
  //   this._OpenOrClose = value;
  // }
  private _switchDisplay: boolean;
  private _isOpenSunday: boolean;

  get display(): boolean {
    return this._switchDisplay;
  }
  constructor(private _formData: FormDataService, private _shopService: ShopService) { }

  @Input()
  set display(value: boolean) {
    this._switchDisplay = value;
  }

  get isOpenSunday(): boolean {
    return this.infosForm.value.openOnSunday ? this._isOpenSunday : !this._isOpenSunday;
  }

  get commentsCtl() {
    return this.infosForm.controls['comments'];
  }
  get daysOpenCtl() {
    return this.infosForm.controls['daysOpen']['controls'];
  }
  get dates() {
    return this.infosForm.get('dates');
  }

  trackTypesHour = (index: number, typeHour: any) => typeHour ? typeHour.id : null;

  // get daysOpenError() {
  //   return this.infosForm.get('daysOpen').hasError('invalidTime');
  // }

  ngOnInit() {
    this.initForm();
    this.selectedTypeHour();
    this.OpenOrClose = false;
    console.log('**********');
    console.warn(this.infosForm.controls);
    console.warn(this.infosForm.get('daysOpen')['controls']);
    console.warn(this.infosForm.controls['daysOpen']['controls']);
    // alert('fsdfsdf');
  }
  initForm() {
    this._shopService.getShop().subscribe(data => {
      this.addInfos = data.addInfos;
      this.infosForm = this._formData.createAddInfoForm(this.addInfos);
    });
  }
  selectedTypeHour() {
    // console.log('************');
    console.log(this.infosForm.get('typeHour'));
    console.log(this.infosForm.get('typeHour').value);
  this._selectTypeHour = this.infosForm.get('typeHour').value;
  // SET VALUE WHEN SELECT DAY TIME OPEN
  //   if (!this.OpenOrClose) this.initForm();
  }
  // SET VALUE ON CLOSE HOUR
  // changeTypeHourForm() {
  //   const controlDay = <FormArray> this.infosForm.get('daysOpen');
  //   console.log(controlDay);
  //   console.log(controlDay.value.firstStartTime );
  //   console.log(addInfos.daysOpen.length);
  //
  //   console.log(this.infosForm.get('daysOpen'));
  //   alert('rezrzer');
  // }


  controlInfos() {
    console.log(this.infosForm);
    const addInfos = this.addInfos;
    if (addInfos.daysOpen !== this.infosForm.get('daysOpen').value)
      addInfos.daysOpen = this.infosForm.get('daysOpen').value;
    if (addInfos.holidayStart !== this.infosForm.get('dates').get('holidayStart').value)
      addInfos.holidayStart = this.infosForm.get('dates').get('holidayStart').value;
    if (addInfos.holidayEnd !== this.infosForm.value.holidayEnd)
      addInfos.holidayEnd = this.infosForm.value.holidayEnd;
    if (addInfos.comments !== this.infosForm.value.comments)
      addInfos.comments = this.infosForm.value.comments;
    if (addInfos.openOnSunday !== this.infosForm.value.openOnSunday)
      addInfos.openOnSunday = this.infosForm.value.openOnSunday;
    if (addInfos.buttonCTA !== this.infosForm.value.buttonCTA)
      addInfos.buttonCTA = this.infosForm.value.buttonCTA;
    if (addInfos.wordsTrader !== this.infosForm.value.wordsTrader)
      addInfos.wordsTrader = this.infosForm.value.wordsTrader;
  }

  printMyForm() {
    console.log(this.infosForm.get('daysOpen'));
  }

}
