import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalInfoFormComponent } from './legal-info-form.component';
import {ReactiveFormsModule} from '@angular/forms';

describe('LegalInfoFormComponent', () => {
  let component: LegalInfoFormComponent;
  let fixture: ComponentFixture<LegalInfoFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
      ],
      declarations: [ LegalInfoFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalInfoFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
