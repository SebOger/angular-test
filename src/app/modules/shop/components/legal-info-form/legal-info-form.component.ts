import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {SHOPS} from '../../mocks/shop-mock';
import {Shop} from '../../models/shop';
import {FormDataService} from '../../services/form-data.service';
import {ShopService} from '../../services/shop.service';
import {LegalInfo} from '../../models/legal-info';

@Component({
  selector: 'app-legal-info-form',
  templateUrl: './legal-info-form.component.html',
  styleUrls: ['./legal-info-form.component.scss']
})
export class LegalInfoFormComponent implements OnInit {

  legalInfosForm: FormGroup;
  private _switchDisplay: boolean;
  legalInfos: LegalInfo;

  constructor(private _formData: FormDataService, private _shopService: ShopService) { }

  get display(): boolean {
    return this._switchDisplay;
  }
  @Input()
  set display(value: boolean) {
    this._switchDisplay = value;
  }
  get legalInfosFormVal() {
    return this.legalInfosForm.value;
  }

  ngOnInit() {
    this._shopService.getShop().subscribe(data => {
      this.legalInfos = data.legalInfos;
    this.legalInfosForm = this._formData.createLegalInfosForm(this.legalInfos);
    });
  }
  controlLegalInfos() {
    if (this.legalInfos.legalEntity !== this.legalInfosFormVal.legalEntity ) { this.legalInfos.legalEntity = this.legalInfosFormVal.legalEntity; }
    if (this.legalInfos.numBceOrTva !== this.legalInfosFormVal.numBceOrTva ) { this.legalInfos.numBceOrTva = this.legalInfosFormVal.numBceOrTva; }
    if (this.legalInfos.ownerName !== this.legalInfosFormVal.ownerName ) { this.legalInfos.ownerName = this.legalInfosFormVal.ownerName; }
    if (this.legalInfos.phoneOwner !== this.legalInfosFormVal.phoneOwner ) { this.legalInfos.phoneOwner = this.legalInfosFormVal.phoneOwner; }
    if (this.legalInfos.emailOwner !== this.legalInfosFormVal.emailOwner ) { this.legalInfos.emailOwner = this.legalInfosFormVal.emailOwner; }
  }
}
