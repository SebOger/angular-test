import {AfterViewInit, Component, ElementRef, Input, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {SHOPS} from '../../mocks/shop-mock';
import {Shop} from '../../models/shop';
import {Image} from '../../models/image';
import * as $ from 'jquery';
import {FormDataService} from '../../services/form-data.service';
import {ShopService} from '../../services/shop.service';
import {Media} from '../../models/media';
declare var require: any;

@Component({
  selector: 'app-media-form',
  templateUrl: './media-form.component.html',
  styleUrls: ['./media-form.component.scss']
})
export class MediaFormComponent implements OnInit {
  mediaForm: FormGroup;
  private _switchDisplay: boolean;
  get display(): boolean {
    return this._switchDisplay;
  }
  @Input()
  set display(value: boolean) {
    this._switchDisplay = value;
  }

  media: Media;
  selectedFile: Image;
  logoImage: Image;
  videoImage: Image;
  ownerImage: Image;
  galleryImage: Image[] = [];

  constructor(private _formData: FormDataService, private _shopService: ShopService) { }

  ngOnInit() {
    this._shopService.getShop().subscribe(data => {
      this.media = data.media;
      this.mediaForm = this._formData.createMediaForm(data.media);
    });
    // console.log(this.shop);
    // console.log(this.media);
    // // this.mediaForm = this._formData.createMediaForm(this.media);
    // console.log('8888888888888888888888888');
    // console.log(this.mediaForm);
    // console.log(this.mediaForm.value);
    this.logoImage = this.media.logo;
    this.videoImage = this.media.video;
    this.ownerImage = this.media.owner;
    this.galleryImage = this.media.gallery;
    console.log(this.logoImage);
    // this.mediaForm = this.fb.group( {
    //   logo: [''],
    //   video: [''],
    //   owner: [''],
    //   gallery: [''],
    // });
    // this.mediaUpdate();
  }
  // mediaUpdate() {
  //
  //   console.log(this.media);
  //   $.each(this.media, function(key, value) {
  //     if (value.src !== undefined)
  //     {
  //       console.log(value.src);
  //       console.log(key, value);
  //     }
  //     if (key === 'gallery') {
  //       console.log(value);
  //     }
  //
  //   });
  //   console.log(this.selectedFile);
  //   this.mediaForm.patchValue({
  //     logo: this.logoImage = this.media.logo,
  //     video: this.videoImage = this.media.video,
  //     owner: this.ownerImage = this.media.owner,
  //     gallery: this.galleryImage = this.media.gallery
  //   });
  //   console.warn(this.media.logo);
  // }

  trackGallery = (index: number, gallery: any) => gallery ? gallery.id : null;

  controlMedia() {
    console.log(this.media);
    console.log(this.mediaForm.value);
    const mediaValues = this.mediaForm.value;
    if (this.media.logo !== mediaValues.logo ) {
      this.media.logo = mediaValues.logo;

    }

    if (this.media.video !== mediaValues.video ) {

      this.media.video = mediaValues.video;
console.log('222222222222');
    }
    console.log(this.media.owner !== mediaValues.owner);

    if (this.media.owner !== mediaValues.owner ) {

      this.media.owner = mediaValues.owner;
console.log('33333333333');
    }
    console.log(this.media.gallery === mediaValues.gallery)
    console.log('-------------');
console.log(this.media.gallery);
//     console.log(this.mediaForm.value);
    console.log(this.mediaForm.value.gallery);
//     console.log(this.media.gallery !== this.mediaForm.value.gallery);
    this.media.gallery.map((data, index ) => {
      console.log(index );
      console.log(data );
      if (data.src !== mediaValues.gallery[index].src ) {
        data = mediaValues.gallery[index];
        console.log(mediaValues.gallery[index]);
        console.log(data);
        console.log(this.media.gallery);
        alert('changed picture');
      }
    });

console.log('fsdfsdfsfsdfds');
    alert('stop');

  }

  processFile(imageInput: any) {
    console.log(imageInput);
    const file: File = imageInput.files[0];
    console.log(file);
    const reader = new FileReader();
    console.log(reader);
    // reader.readAsDataURL()
    reader.addEventListener('load', (event: any) => {
      console.log(event);
      this.selectedFile = new Image(6, event.target.result, file);
      console.log(this.selectedFile);
      console.log('**************************');
      // console.log(this.media.logo);

      console.log('**************************');
      console.log(imageInput);
      switch (imageInput.id) {
        case 'logo' :
          if (this.media.logo.file === undefined ||
            this.selectedFile.file.name !== this.media.logo.file.name) {
            this.media.logo = this.logoImage = this.selectedFile;
          }
          break;
        case 'video' :
          if (this.media.video.file === undefined ||
            this.selectedFile.file.name !== this.media.video.file.name) {
            this.media.video =  this.videoImage = this.selectedFile;
          }
          break;
        case 'owner' :
          if (this.media.owner.file === undefined ||
            this.selectedFile.file.name !== this.media.owner.file.name) {
            this.media.owner =  this.ownerImage = this.selectedFile;
          }
          break;
        default: console.log('NULL');
          break;
      }

        const entryEntity = imageInput.id.slice(8);
      console.log(this.media.gallery);
      console.log(this.media.gallery[entryEntity]);
      console.log('--****************//////////////////////');
      if (this.media.gallery[entryEntity] !== undefined)
      {
        if (this.media.gallery[entryEntity].file === undefined ||
          this.selectedFile.file.name !== this.media.gallery[entryEntity].file.name)
        {
          if (imageInput.id.substring(0, 7) === 'gallery')
          {
            this.galleryImage[entryEntity] = this.selectedFile;
            this.media.gallery[entryEntity] = this.selectedFile;
          }
        } else {
          console.log('OUUTUTUTUTUTUTUTU');
        }
      }

      console.warn('---------------');
      console.log(this.galleryImage);
      console.log(this.media.gallery);
      console.warn('---------------warn');
    });
    reader.readAsDataURL(file);
  }
}
