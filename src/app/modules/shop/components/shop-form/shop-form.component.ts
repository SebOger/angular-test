import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {SHOPS} from '../../mocks/shop-mock';
import {Shop} from '../../models/shop';
import {Sector} from '../../models/sector';
import {SubSector} from '../../models/sub-sector';
import {ShopService} from '../../services/shop.service';
import {FormDataService} from '../../services/form-data.service';

@Component({
  selector: 'app-shop-form',
  templateUrl: './shop-form.component.html',
  styleUrls: ['./shop-form.component.scss']
})
export class ShopFormComponent implements OnInit, AfterViewInit {
  // TO SET THE FOCUS WHEN EDITING
  @ViewChild('nameRef') nameElementRef: ElementRef;
  private _switchDisplay: boolean;

  get display(): boolean {
    return this._switchDisplay;
  }
  @Input()
  set display(value: boolean) {
    this._switchDisplay = value;
  }
  shopDataForm: FormGroup;

  // CONTROL VAR TO CHANGE DISPLAY OR INPUTS INTO VIEW
  // DEFINE shop FROM SHOP MOCK
  shop: Shop;
  sectors: Sector[];
  subSectors: SubSector[];

  constructor(private fb: FormBuilder,
              private _shopService: ShopService,
              private _formData: FormDataService) {
  }

  get name() { return this.shopDataForm.get('name'); }
  get adress() { return this.shopDataForm.get('adress'); }
  get zipCode() { return this.shopDataForm.get('zipCode'); }
  get town() { return this.shopDataForm.get('town'); }
  get mobilePhone() { return this.shopDataForm.get('mobilePhone'); }
  get phone() { return this.shopDataForm.get('phone'); }
  get fax() { return this.shopDataForm.get('fax'); }
  get contactName() { return this.shopDataForm.get('contactName'); }
  get sector() { return this.shopDataForm.get('sectorsGroup').get('sector'); }
  get subSector() { return this.shopDataForm.get('sectorsGroup').get('subSector'); }
  trackSectors = (index: number, sector: any) => sector ? sector.id : null;
  trackSubSectors = (index: number, subSector: any) => subSector ? subSector.id : null;

  ngOnInit() {
    this.getShop();
    this.getSector();
    this.getSubSectors();
    console.log(this.shop);
  }

  ngAfterViewInit()  {
    // SET FOCUS ON FIRST INPUT WHEN EDIT
    console.log(this.nameElementRef);
    this.nameElementRef.nativeElement.focus();
  }

  onChange(sectorSelected: string) {
    console.log(this.shopDataForm.get('sectorsGroup'));
    console.log(sectorSelected.split(':'));
     console.log(this.shop.sector);
    const sectorId = parseInt(sectorSelected.split(':')[0], 10);
    this.shop.sector = this._shopService.getSectorId(sectorId);
    this.getSubSectors(sectorId);
    console.log(this.shopDataForm.get('sectorsGroup'));
    this.shopDataForm.get('sectorsGroup').patchValue({subSector: ''});
    // console.log(this.shopDataForm.get('sectorsGroup'));
    // this.shopDataForm.get('sectorsGroup').patchValue({subSector: 'Select'});
    alert('fsdfsdfs');
console.log('*************');
  }
  getShop() {
    this._shopService.getShop().subscribe(data => {
      this.shop = data;
      this.shopDataForm = this._formData.createShopForm(this.shop);
    });
  }
  getSector(): void {
    this._shopService.getSectors()
      .subscribe(sectors => this.sectors = sectors);
  }

  getSubSectors( id?: number): void {
    (this.subSector.value === '') ?
      this._shopService.getSubSectors()
        .subscribe(subSectors => this.subSectors = subSectors) :
    this._shopService.getSubSectorsBySector(this.shop.sector, id )
      .subscribe(subSectors => this.subSectors = subSectors);
  }

  controlShop() {
    if (this.shopDataForm.touched ) {
      if (this.shop.name !== this.shopDataForm.value.name ) this.shop.name = this.shopDataForm.value.name;
      if (this.shop.adress !== this.shopDataForm.value.adress ) { this.shop.adress = this.shopDataForm.value.adress; }
      if (this.shop.zipCode !== this.shopDataForm.value.zipCode ) { this.shop.zipCode = this.shopDataForm.value.zipCode; }
      if (this.shop.town !== this.shopDataForm.value.town ) this.shop.town = this.shopDataForm.value.town;
      if (this.shop.phone !== this.shopDataForm.value.phone ) this.shop.phone = this.shopDataForm.value.phone;
      if (this.shop.mobilePhone !== this.shopDataForm.value.mobilePhone ) this.shop.mobilePhone = this.shopDataForm.value.mobilePhone;
      if (this.shop.fax !== this.shopDataForm.value.fax ) this.shop.fax = this.shopDataForm.value.fax;
      if (this.shop.email !== this.shopDataForm.value.email ) this.shop.email = this.shopDataForm.value.email;
      if (this.shop.webSite !== this.shopDataForm.value.webSite ) this.shop.webSite = this.shopDataForm.value.webSite;
      if (this.shop.contactName !== this.shopDataForm.value.contactName ) this.shop.contactName = this.shopDataForm.value.contactName;
      if (this.shop.sector.name !== this.shopDataForm.value.sectorsGroup.sector ) this.shop.sector.name = this.shopDataForm.value.sectorsGroup.sector;
      if (this.shop.subSector.name !== this.shopDataForm.value.sectorsGroup.subSector ) this.shop.subSector.name = this.shopDataForm.value.sectorsGroup.subSector;
      if (this.shop.description !== this.shopDataForm.value.description ) this.shop.description = this.shopDataForm.value.description;
      if (this.shop.townCenter !== this.shopDataForm.value.townCenter ) this.shop.townCenter = this.shopDataForm.value.townCenter;
      if (this.shop.accessPMR !== this.shopDataForm.value.accessPMR ) this.shop.accessPMR = this.shopDataForm.value.accessPMR;
    }

  }
}
