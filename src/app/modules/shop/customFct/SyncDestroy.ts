import {catchError, finalize, shareReplay} from 'rxjs/operators';
import {defer, EMPTY, Observable} from 'rxjs';

export const syncDestroy = (): any => {
  return source => defer((): Observable<any> => {
    return source.pipe(
      catchError(error => {
        console.error(error);
        return EMPTY;
      }),
      finalize(() => {
        console.log('Done!');
      }),
      shareReplay(1)
    );
  });
}

