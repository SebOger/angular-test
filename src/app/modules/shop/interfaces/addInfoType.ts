import {DayOpen} from '../models/add-infos/day-open';

export interface AddInfoType {
  id: number;
  daysOpen: DayOpen[];
  holidayStart: Date | string;
  holidayEnd: Date | string;
  comments: string;
  openOnSunday: boolean;
  buttonCTA: string;
  wordsTrader: string;
}
