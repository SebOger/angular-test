export interface DayOpenType {
  id: number;
  day: string;
  firstStartTime: string;
  firstEndTime: string;
  secondStartTime: string;
  secondEndTime: string;
  rdv: boolean;
  midday: boolean;
}
