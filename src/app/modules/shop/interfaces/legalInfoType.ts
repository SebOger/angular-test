export interface LegalInfoType {
  id: number;
  legalEntity: string;
  numBceOrTva: number;
  ownerName: string;
  phoneOwner: string;
  emailOwner: string;
}
