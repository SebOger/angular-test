import {Image} from '../models/image';

export interface MediaType {
  id: number;
  logo: Image;
  video: Image;
  owner: Image;
  gallery: Array<Image>;
}
