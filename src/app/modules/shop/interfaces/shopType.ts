import {Sector} from '../models/sector';
import {SubSector} from '../models/sub-sector';
import {AddInfo} from '../models/add-infos/add-info';
import {Media} from '../models/media';
import {LegalInfo} from '../models/legal-info';

export interface ShopType {
  id: number;
  name: string;
  adress: string;
  zipCode: number;
  town: string;
  phone: string;
  mobilePhone: string;
  fax: string;
  email: string;
  webSite: string;
  contactName: string;
  sector: Sector;
  subSector: SubSector;
  description: string;
  townCenter: boolean;
  accessPMR: boolean;
  addInfos: AddInfo;
  media: Media;
  legalInfos: LegalInfo;
}
