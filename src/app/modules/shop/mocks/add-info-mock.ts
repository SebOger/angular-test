import {AddInfo} from '../models/add-infos/add-info';
import {DAYS_OPEN} from './day-open-mock';

export const ADD_INFOS =
  new AddInfo(1, DAYS_OPEN,
    new Date('10/30/2019'),
    new Date('10/31/2019'),
    'Commentaire ...',
    true,
    'Contactez-nous',
    'Je prends mes congés annuels blablabla');

