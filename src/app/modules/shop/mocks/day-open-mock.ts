import {DayOpen} from '../models/add-infos/day-open';
export const DAYS_OPEN: DayOpen[] =
  [
    new DayOpen(0, 'Lu',  '8', '12',  '13:15',  '16:30',  false,  true),
    new DayOpen( 1, 'Ma',  '8', '12',  '13:15',  '16:30',  false,  true),
    new DayOpen( 2, 'Me',  '8', '12',  '13:15',  '16:30',  false,  true),
    new DayOpen( 3, 'Je',  '8', '12',  '13:15',  '16:30',  false,  true),
    new DayOpen( 4, 'Ve',  '8', '12',  '13:15',  '16:30',  false,  true),
    new DayOpen( 5, 'Sa',  '8', '12',  '',  '',  true,  false),
    new DayOpen( 6, 'Di', '', '',  '',  '',  true,  false),
];
