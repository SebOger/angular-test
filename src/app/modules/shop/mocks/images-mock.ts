import {Image} from '../models/image';

export const IMAGES: Image[] = [
  new Image(1, '../../../../../assets/icons/down-arrow.png'),
  new Image(2, ''),
  new Image(3, '../../../../../assets/img/Photo-magasin-Uccle-2-665x300px.png'),
  new Image(4, '../../../../../assets/img/DSCN0011.png'),
  new Image(5, '../../../../../assets/img/magasin_in2.jpg'),
  new Image(6, '../../../../../assets/img/barentin_1_1.jpg'),
  new Image(7, '../../../../../assets/img/le-magasin-general-le-brun-boutique-du-magasin-general.jpg'),
  new Image(8, '../../../../../assets/img/551103-1TOqFD1502285018.jpg'),
  new Image(9, '../../../../../assets/icons/add.png'),
];
