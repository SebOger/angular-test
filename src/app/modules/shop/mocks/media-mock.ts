import {Media} from '../models/media';
import {IMAGES} from './images-mock';

export const MEDIAS =
  new Media(5, IMAGES[7], IMAGES[1], IMAGES[1],
    [IMAGES[2], IMAGES[1], IMAGES[4], IMAGES[5], IMAGES[2], IMAGES[6]]
  );

// export const MEDIAS: Media[] = [
//   {
//     id: 5,
//     logo: IMAGES[0],
//     video: IMAGES[1],
//     pictureTrader: IMAGES[2],
//     gallery: [IMAGES[1], IMAGES[2], IMAGES[2], IMAGES[1], IMAGES[2], IMAGES[1]]
//   }
// ];
