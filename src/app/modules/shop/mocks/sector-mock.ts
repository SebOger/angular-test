import {Sector} from '../models/sector';

export const SECTORS: Sector[] =
  [
  new Sector( 1,  'Construction'),
  new Sector(2,  'Informatique'),
  new Sector( 3,  'Grossiste'),
  new Sector( 4,  'order 4')
  ];
