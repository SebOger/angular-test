import {Shop} from '../models/shop';
import {ADD_INFOS} from './add-info-mock';
import {MEDIAS} from './media-mock';
import {LEGAL_INFOS} from './legal-info-mock';
import {SECTORS} from './sector-mock';
import {SUB_SECTORS} from './sub-sector-mock';

export const SHOPS = new Shop(
  1 ,
  'App& Web',
  'Rue du Commerce, n°19',
  5000,
  'Marche en Famène',
  '081007007',
  '0488291507',
  '081424242',
  'appandweb@vdfrer.com',
  'www.appandweb.be',
  'Maxime',
  SECTORS[1],
  SUB_SECTORS[5],
  'Description blabla',
  true,
  false,
  ADD_INFOS,
  MEDIAS,
  LEGAL_INFOS
  );
