import {SubSector} from '../models/sub-sector';

export const SUB_SECTORS: SubSector[] =
  [
    new SubSector(1, 1, 'menuiserie'),
    new SubSector(2, 1, 'maconnerie'),
    new SubSector(3, 1, 'renovation'),
    new SubSector(4, 2, 'programmation'),
    new SubSector(5, 2, 'réseau'),
    new SubSector(6, 2, 'conception web'),
    new SubSector(7, 3, 'produits épicerie'),
    new SubSector(8, 3, 'prodtuis entretien'),
  ]
