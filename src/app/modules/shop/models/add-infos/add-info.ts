import {DayOpen} from './day-open';
import {formatDate, toDateStr} from '../../customFct/DateStr';

export class AddInfo {
  private _id: number;
  private _daysOpen: DayOpen[];
  private _holidayStart: Date | string;
  private _holidayEnd: Date | string;
  private _comments: string;
  private _openOnSunday: boolean;
  private _buttonCTA: string;
  private _wordsTrader: string;

constructor(id: number, daysOpen: DayOpen[], holidayStart: Date | string,
            holidayEnd: Date | string, comments: string, openOnSunday: boolean, buttonCTA: string, wordsTrader: string) {
  this._id = id;
  this._daysOpen = daysOpen;
  this._holidayStart = formatDate(toDateStr(holidayStart));
  this._holidayEnd = formatDate(toDateStr(holidayEnd));
  this._comments = comments;
  this._openOnSunday = openOnSunday;
  this._buttonCTA = buttonCTA;
  this._wordsTrader = wordsTrader;
}

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get daysOpen(): DayOpen[] {
    return this._daysOpen;
  }

  set daysOpen(value: DayOpen[]) {
    this._daysOpen = value;
  }

  get holidayStart(): Date | string {
    return this._holidayStart;
  }

  set holidayStart(value: Date | string) {
    this._holidayStart = value;
  }

  get holidayEnd(): Date | string {
    return this._holidayEnd;
  }

  set holidayEnd(value: Date | string) {
    this._holidayEnd = value;
  }

  get comments(): string {
    return this._comments;
  }

  set comments(value: string) {
    this._comments = value;
  }

  get openOnSunday(): boolean {
    return this._openOnSunday;
  }

  set openOnSunday(value: boolean) {
    this._openOnSunday = value;
  }

  get buttonCTA(): string {
    return this._buttonCTA;
  }

  set buttonCTA(value: string) {
    this._buttonCTA = value;
  }

  get wordsTrader(): string {
    return this._wordsTrader;
  }

  set wordsTrader(value: string) {
    this._wordsTrader = value;
  }
}
