
export class DayOpen {
  private _id: number;
  private _day: string;
  private _firstStartTime: string;
  private _firstEndTime: string;
  private _secondStartTime: string;
  private _secondEndTime: string;
  private _rdv: boolean;
  private _midday: boolean;

  constructor(id: number, day: string, firstStartTime: string, firstEndTime: string,
              secondStartTime: string, secondEndTime: string, rdv: boolean, midday: boolean)
  {
    this._id = id;
    this._day = day;
    // this._firstStartTime =  DayOpen.checkEmptyDay(firstStartTime);
    this._firstStartTime =  DayOpen.checkEmptyDay(firstStartTime);
    this._firstEndTime =  DayOpen.checkEmptyDay(firstEndTime);
    this._secondStartTime =  DayOpen.checkEmptyDay(secondStartTime);
    this._secondEndTime =  DayOpen.checkEmptyDay(secondEndTime);
    this._rdv = rdv;
    this._midday = midday;
  }

  static checkEmptyDay(value: string): string {
    return (value !== '') ? ((value.length <= 2 ) ? value + ':00' : value ) : ':';
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get day(): string {
    return this._day;
  }

  set day(value: string) {
    this._day = value;
  }

  get firstStartTime(): string {
    return this._firstStartTime;
  }

  set firstStartTime(value: string) {
    this._firstStartTime = value;
  }

  get firstEndTime(): string {
    return this._firstEndTime;
  }

  set firstEndTime(value: string) {
    this._firstEndTime = value;
  }

  get secondStartTime(): string {
    return this._secondStartTime;
  }

  set secondStartTime(value: string) {
    this._secondStartTime = value;
  }

  get secondEndTime(): string {
    return this._secondEndTime;
  }

  set secondEndTime(value: string) {
    this._secondEndTime = value;
  }

  get rdv(): boolean {
    return this._rdv;
  }

  set rdv(value: boolean) {
    this._rdv = value;
  }

  get midday(): boolean {
    return this._midday;
  }

  set midday(value: boolean) {
    this._midday = value;
  }
}
