
export class Image {
  private _id: number;
  private _src: string;
  private _file: File;
  constructor(id: number, src: string, file?: File) {
    this._id = id;
    this._src = src;
    this._file = file;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get file(): File {
    return this._file;
  }

  set file(value: File) {
    this._file = value;
  }

  get src(): string {
    if(this._src === '') this._src = '../../assets/img/not.png';
    return this._src;
  }

  set src(value: string) {
    this._src = value;
  }
}
