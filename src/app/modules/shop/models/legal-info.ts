export class LegalInfo {

  private _id: number;
  private _legalEntity: string;
  private _numBceOrTva: number;
  private _ownerName: string;
  private _phoneOwner: string;
  private _emailOwner: string;

  constructor(id: number, legalEntity: string, numBceOrTva: number, ownerName: string, phoneOwner: string, emailOwner: string)
  {
    this._id = id;
    this._legalEntity = legalEntity;
    this._numBceOrTva = numBceOrTva;
    this._ownerName = ownerName;
    this._phoneOwner = phoneOwner;
    this._emailOwner = emailOwner;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get legalEntity(): string {
    return this._legalEntity;
  }

  set legalEntity(value: string) {
    this._legalEntity = value;
  }

  get numBceOrTva(): number {
    return this._numBceOrTva;
  }

  set numBceOrTva(value: number) {
    this._numBceOrTva = value;
  }

  get ownerName(): string {
    return this._ownerName;
  }

  set ownerName(value: string) {
    this._ownerName = value;
  }

  get phoneOwner(): string {
    return this._phoneOwner;
  }

  set phoneOwner(value: string) {
    this._phoneOwner = value;
  }

  get emailOwner(): string {
    return this._emailOwner;
  }

  set emailOwner(value: string) {
    this._emailOwner = value;
  }
}
