import {Image} from './image';

export class Media {
  private _id: number;
  private _logo: Image;
  private _video: Image;
  private _owner: Image;
  private _gallery: Array<Image>;
  constructor(id: number, logo: Image, video: Image, owner: Image, gallery: Array<Image>)
  {
    this._id = id;
    this._logo = logo;
    this._video = video;
    this._owner = owner;
    this._gallery = gallery;
  }
  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get logo(): Image {
    return this._logo;
  }

  set logo(value: Image) {
    this._logo = value;
  }

  get video(): Image {
    return this._video;
  }

  set video(value: Image) {
    this._video = value;
  }

  get owner(): Image {
    return this._owner;
  }

  set owner(value: Image) {
    this._owner = value;
  }

  get gallery(): Array<Image> {
    return this._gallery;
  }

  set gallery(value: Array<Image>) {
    this._gallery = value;
  }
}
