import {Media} from './media';
import {LegalInfo} from './legal-info';
import {AddInfo} from './add-infos/add-info';
import {Sector} from './sector';
import {SubSector} from './sub-sector';

export class Shop {
  private _id: number;
  private _name: string;
  private _adress: string;
  private _zipCode: number;
  private _town: string;
  private _phone: string;
  private _mobilePhone: string;
  private _fax: string;
  private _email: string;
  private _webSite: string;
  private _contactName: string;
  private _sector: Sector;
  private _subSector: SubSector;
  private _description: string;
  private _townCenter: boolean;
  private _accessPMR: boolean;
  private _addInfos: AddInfo;
  private _media: Media;
  private _legalInfos: LegalInfo;

  constructor(
    id: number, name: string, adress: string, zipCode: number, town: string,
    phone: string, mobilePhone: string, fax: string, email: string, webSite: string,
    contactName: string, sector: Sector, subSector: SubSector, description: string,
    townCenter: boolean, accessPMR: boolean, addInfos: AddInfo, media: Media, legalInfos: LegalInfo
  ) {
  this._id = id;
  this._name = name;
  this._adress = adress;
  this._zipCode = zipCode;
  this._town = town;
  this._phone = phone;
  this._mobilePhone = mobilePhone;
  this._fax = fax;
  this._email = email;
  this._webSite = webSite;
  this._contactName = contactName;
  this._sector = sector;
  this._subSector = subSector;
  this._description = description;
  this._townCenter = townCenter;
  this._accessPMR = accessPMR;
  this._addInfos = addInfos;
  this._media = media;
  this._legalInfos = legalInfos;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }

  get adress(): string {
    return this._adress;
  }

  set adress(value: string) {
    this._adress = value;
  }

  get zipCode(): number {
    return this._zipCode;
  }

  set zipCode(value: number) {
    this._zipCode = value;
  }

  get town(): string {
    return this._town;
  }

  set town(value: string) {
    this._town = value;
  }

  get phone(): string {
    return this._phone;
  }

  set phone(value: string) {
    this._phone = value;
  }

  get mobilePhone(): string {
    return this._mobilePhone;
  }

  set mobilePhone(value: string) {
    this._mobilePhone = value;
  }

  get fax(): string {
    return this._fax;
  }

  set fax(value: string) {
    this._fax = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get webSite(): string {
    return this._webSite;
  }

  set webSite(value: string) {
    this._webSite = value;
  }

  get contactName(): string {
    return this._contactName;
  }

  set contactName(value: string) {
    this._contactName = value;
  }

  get sector(): Sector {
    return this._sector;
  }

  set sector(value: Sector) {
    this._sector = value;
  }

  get subSector(): SubSector {
    return this._subSector;
  }

  set subSector(value: SubSector) {
    this._subSector = value;
  }

  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }

  get townCenter(): boolean {
    return this._townCenter;
  }

  set townCenter(value: boolean) {
    this._townCenter = value;
  }

  get accessPMR(): boolean {
    return this._accessPMR;
  }

  set accessPMR(value: boolean) {
    this._accessPMR = value;
  }

  get addInfos(): AddInfo {
    return this._addInfos;
  }

  set addInfos(value: AddInfo) {
    this._addInfos = value;
  }

  get media(): Media {
    return this._media;
  }

  set media(value: Media) {
    this._media = value;
  }

  get legalInfos(): LegalInfo {
    return this._legalInfos;
  }

  set legalInfos(value: LegalInfo) {
    this._legalInfos = value;
  }
}
