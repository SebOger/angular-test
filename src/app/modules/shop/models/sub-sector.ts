
export class SubSector {
  private _id: number;
  private _sectorId: number;
  private _name: string;

  constructor(id: number, sectorId: number, name: string) {
    this._id = id;
    this._sectorId = sectorId;
    this._name = name;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get sectorId(): number  {
    return this._sectorId;
  }

  set sectorId(value: number) {
    this._sectorId = value;
  }

  get name(): string {
    return this._name;
  }

  set name(value: string) {
    this._name = value;
  }
}
