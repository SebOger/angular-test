import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({
  name: 'formatDate'
})
export class FormatDatePipe implements PipeTransform {
  transform(date: any, args?: any): any {
    console.warn(date);
    const d = new Date(date);
    return moment(d).format('YYYY-MM-DD');
  }
}
