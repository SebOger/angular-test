import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
// GET DATA TO INJECT INTO FOR A OTHER COMPONENT
export class DataStorageService {

  data: any;
  constructor() { }
}
