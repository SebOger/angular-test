import { Injectable } from '@angular/core';
import { FormBuilder, Validators} from '@angular/forms';
import {CustomValidator} from '../validator/custom-validator';
import {DayOpenType} from '../interfaces/dayOpenType';
import {ShopType} from '../interfaces/shopType';
import {MediaType} from '../interfaces/mediaType';
import {LegalInfoType} from '../interfaces/legalInfoType';
import {TYPE_HOURS} from '../mocks/type-hours';
import {AddInfoType} from '../interfaces/addInfoType';

@Injectable({
  providedIn: 'root'
})
export class FormDataService {

  constructor(private fb: FormBuilder) { }

  createShopForm(apiResponse: ShopType) {
    return this.fb.group({
      name: [ apiResponse.name, Validators.required],
      adress: [ apiResponse.adress, Validators.required],
      zipCode: [ apiResponse.zipCode, [
        Validators.required,
        CustomValidator.zipCodeValidator]],
      town: [ apiResponse.town],
      phone: [ apiResponse.phone, [CustomValidator.phoneValidator]],
      mobilePhone: [ apiResponse.mobilePhone, [
        Validators.required,
        CustomValidator.mobileValidator]],
      fax: [ apiResponse.fax, [CustomValidator.phoneValidator]],
      email: [ apiResponse.email, [
        Validators.required,
        Validators.email]],
      webSite: [ apiResponse.webSite, [CustomValidator.urlValidator]],
      contactName: [ apiResponse.contactName, Validators.required],
      sectorsGroup: this.fb.group({
        sector: [ apiResponse.sector.name],
        subSector: [ apiResponse.subSector.name],
      }),
      description: [ apiResponse.description, Validators.required],
      townCenter: [ apiResponse.townCenter],
      accessPMR: [ apiResponse.accessPMR]
    });
  }

  createAddInfoForm( apiResponse: AddInfoType) {
    console.warn('*******************************');
    console.warn('*******************************');
console.warn(apiResponse);
    console.warn('*******************************');
    console.warn('*******************************');
    return this.fb.group({
      typeHour: TYPE_HOURS[1].name,
      daysOpen: this.fb.array(
        apiResponse.daysOpen.map(dayOpen => this.createDaysArray(dayOpen))),
      dates: this.fb.group({
        holidayStart: [ apiResponse.holidayStart ],
        holidayEnd: [ apiResponse.holidayEnd]
      }, {validator: CustomValidator.datesValidator}),
      comments: [ apiResponse.comments, [Validators.maxLength(50)] ],
      openOnSunday: [ apiResponse.openOnSunday ],
      buttonCTA: [ apiResponse.buttonCTA, [Validators.maxLength(20)] ],
      wordsTrader: [ apiResponse.wordsTrader ],
    });
  }

  createDaysArray(day: DayOpenType) {
    return this.fb.group({
        day :  [ day.day, false ],
        firstStartTime: [ day.firstStartTime, {validators: [CustomValidator.timeValidator], updateOn: 'blur'} ],
        firstEndTime: [ day.firstEndTime, {validators: [CustomValidator.timeValidator], updateOn: 'blur'} ],
        secondStartTime: [ day.secondStartTime, {validators: [CustomValidator.timeValidator], updateOn: 'blur'} ],
        secondEndTime: [ day.secondEndTime, {validators: [CustomValidator.timeValidator], updateOn: 'blur'} ],
        rdv: [ day.rdv ],
        midday: [ day.midday ],
      });
  }

  createMediaForm(apiResponse: MediaType) {
    return this.fb.group({
      logo: apiResponse.logo,
      video: apiResponse.video,
      owner: apiResponse.owner,
      gallery: this.fb.array(
        apiResponse.gallery.map(data => data))
    });
  }

  createLegalInfosForm(apiResponse: LegalInfoType) {
    return this.fb.group({
      legalEntity: apiResponse.legalEntity,
      numBceOrTva: apiResponse.numBceOrTva,
      ownerName: apiResponse.ownerName,
      phoneOwner: apiResponse.phoneOwner,
      emailOwner: apiResponse.emailOwner
    });
  }
}
