import { Injectable } from '@angular/core';
import {Shop} from '../models/shop';
import {SHOPS} from '../mocks/shop-mock';
import { Observable, of} from 'rxjs';
import {Sector} from '../models/sector';
import {SECTORS} from '../mocks/sector-mock';
import {SubSector} from '../models/sub-sector';
import {SUB_SECTORS} from '../mocks/sub-sector-mock';
import {syncDestroy} from '../customFct/SyncDestroy';
import {Media} from '../models/media';

@Injectable({
  providedIn: 'root'
})
export class ShopService {

  shop: Shop = SHOPS;
  sectors: Sector[] = SECTORS;
  subSectors: SubSector[] = SUB_SECTORS;

  constructor() { }

  getShop(): Observable<Shop> {
    return of(this.shop)
      .pipe(syncDestroy());
  }

  getSectors(): Observable<Sector[]> {
    return of(this.sectors)
      .pipe(syncDestroy());
  }

  getSectorId(sectorId: number): any {
    return of(this.sectors.filter(sector => sector.id === sectorId))
      .pipe(syncDestroy());
  }

  getSubSectors(): Observable<SubSector[]> {
    return of(this.subSectors)
      .pipe(syncDestroy());
  }

  getSubSectorsBySector(mySector: Sector, id: number): Observable<SubSector[]> {
    function findSubSectById(element) {
        return (element.sectorId === ((id === undefined ) ? mySector.id : id));
    }
    return of(this.subSectors.filter(findSubSectById))
      .pipe(syncDestroy());
  }
}
