import {Component, OnInit, ViewChild} from '@angular/core';
import {ShopFormComponent} from './components/shop-form/shop-form.component';
import {AddInfoFormComponent} from './components/add-info-form/add-info-form.component';
import {MediaFormComponent} from './components/media-form/media-form.component';
import {LegalInfoFormComponent} from './components/legal-info-form/legal-info-form.component';
import {Shop} from './models/shop';
import {ShopService} from './services/shop.service';
import {FormGroup} from '@angular/forms';
import * as $ from 'jquery';
import {Sector} from './models/sector';
import {catchError, finalize, shareReplay} from 'rxjs/operators';
import {EMPTY} from 'rxjs';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {
  @ViewChild(ShopFormComponent) shopComponent: ShopFormComponent;
  @ViewChild(AddInfoFormComponent) infosComponent: AddInfoFormComponent;
  @ViewChild(MediaFormComponent) mediaComponent: MediaFormComponent;
  @ViewChild(LegalInfoFormComponent) legalInfoComponent: LegalInfoFormComponent;
  switchDisplay: boolean;
  msgErrors: string[];

  ngOnInit() {
    // this.getShop();
    // console.log(this.shop);
    console.log('ééééééééééééééééééééééééééééééééééééééééé');
    // DEFINIE EDIT / DISPLAY
    this.switchDisplay = true;
    $(document).ready(function() {

      $.fn.rotation = function () {
        const angle = ( $(this).data('angle') + 180) || 180;
        $(this).css({'transition': 'all 1s ease'});
        $(this).css({'transform': 'rotateX(' + angle + 'deg)'});
        $(this).css({ 'WebkitTransform': 'rotateX(' + angle + 'deg)'});
        $(this).css({ '-moz-transform': 'rotateX(' + angle + 'deg)'});
        $(this).data('angle', angle);
        return $(this);
      }

      $('div.title').on('click', function() {
        let $img = $(this).find('img');
        const $select = $(this).siblings().hasClass('content') ?
          $(this).siblings() : $(this).siblings().find('.content');
        $select.toggle('slow');
        $img.rotation();
      });
    });
  }

  // getShop(): void {
  //   this._shopService.getShop()
  //     .subscribe(shop => this.shop = shop);
  // }
  //

  // POUR TROUVER INPUT INVALID DANS FORM
  public findInvalidControls(form: FormGroup): string[] {
    const invalid = [];
    console.log(form);
    const controls = form.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name + ' erreur');
      }
      console.log('JJJJJJJJ');
    }
    return invalid;
  }

  showShop() {
    if (this.switchDisplay) {
      // this.submitted = true;
      console.log('showShop');
      console.log(this.shopComponent.shopDataForm);
      console.log(this.infosComponent.infosForm);
      if (this.shopComponent.shopDataForm.invalid || this.infosComponent.infosForm.invalid
        || this.mediaComponent.mediaForm.invalid || this.legalInfoComponent.legalInfosForm.invalid) {
        console.log('INVALIDE');
        console.log(this);
        console.log('------------------');
        // RETURN ERROR MSG FROM THE INVALID FIELDS FORMS CHILD FOR THE MAIN VIEW
        this.msgErrors = this.findInvalidControls(this.shopComponent.shopDataForm);
        console.log( this.findInvalidControls(this.shopComponent.shopDataForm));
        console.log(this.shopComponent.shopDataForm.controls);
        console.log(this.msgErrors);
        return;
      }
      alert('fsdfsdf');
        // delete this.msgErrors;
        // this.switchDisplay = false;
        this.shopComponent.controlShop();
        this.infosComponent.controlInfos();
        this.mediaComponent.controlMedia();
        this.legalInfoComponent.controlLegalInfos();
      console.log(this.switchDisplay);
      // return this.switchDisplay;
    }
    return this.switchDisplay ? this.switchDisplay = false : null;
  }
  editShop() {
    return !this.switchDisplay ? this.switchDisplay = true : null;
  }


}
