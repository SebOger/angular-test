import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ShopService} from './services/shop.service';
import {ShopFormComponent} from './components/shop-form/shop-form.component';
import {AddInfoFormComponent} from './components/add-info-form/add-info-form.component';
import {MediaFormComponent} from './components/media-form/media-form.component';
import {LegalInfoFormComponent} from './components/legal-info-form/legal-info-form.component';
import {FormatDatePipe} from './pipes/format-date.pipe';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ShopComponent } from './shop.component';

@NgModule({
  imports: [
    CommonModule,
    // FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    ShopComponent,
    ShopFormComponent,
    AddInfoFormComponent,
    MediaFormComponent,
    LegalInfoFormComponent,
    FormatDatePipe,
  ],
  providers: [ShopService]
})
export class ShopModule { }
