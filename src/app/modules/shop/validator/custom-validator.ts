import {AbstractControl} from '@angular/forms';
import * as moment from 'moment';

export class CustomValidator {
// Validates URL
  static urlValidator(url): any {
    if (url.pristine) {
      return null;
    }

    const URL_REGEXP = /^(http?|https):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;

    url.markAsTouched();

    if (URL_REGEXP.test(url.value)) {
      return null;
    }

    return {
      invalidUrl: true
    };
  }

// Validates BE phone numbers
  static phoneValidator(number): any {
    if (number.pristine) {
      return null;
    }

    const PHONE_REGEXP = /^((\+|00)32\s?|0)(\d\s?\d{3}|\d{2}\s?\d{2})(\s?\d{2}){2}$/;

    number.markAsTouched();

    if (PHONE_REGEXP.test(number.value)) {
      return null;
    }

    return {
      invalidPhone: true
    };
  }

  // Validates BE phone numbers
  static mobileValidator(number): any {
    if (number.pristine) {
      return null;
    }

    const MOBILE_REGEXP = /^((\+|00)32\s?|0)4(60|[789]\d)(\s?\d{2}){3}$/;
    number.markAsTouched();

    if (MOBILE_REGEXP.test(number.value)) {
      return null;
    }

    return {
      invalidMobile: true
    };
  }

// Validates zip codes
  static zipCodeValidator(zip): any {
    if (zip.pristine) {
      return null;
    }

    const ZIP_REGEXP = /^[0-9]{4}(?:-[0-9]{4})?$/;

    zip.markAsTouched();

    if (ZIP_REGEXP.test(zip.value)) {
      return null;
    }

    return {
      invalidZip: true
    };
  }
  static timeValidator(control: AbstractControl): {[key: string]: any} | null {
    console.log('(((((CUSTOMMMMMM(((((((((');
    console.warn(control);
    console.log(control.value);
    // if (control.pristine) {
    //   return null;
    // }
    //   const TIME_REGEXP = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])|:$/;
      const TIME_REGEXP = /^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]$/;
    const NULL_REGEXP = /^:$/;
    const HOURS_REGEXP = /^[0-9]:$/;
    // if (HOURS_REGEXP.test(control.value)) alert(control.value);
    console.log(TIME_REGEXP.test(control.value));
      if (NULL_REGEXP.test(control.value) || TIME_REGEXP.test(control.value)) {
        return null;
      } else {
        control.setValue(':');
        alert('NNNNNNNNNNNOOOOOOOOOOOOO');
        console.log('((((( END CUSTOMMMMMM(((((((((');
        return {
          invalidTime: true
        };
      }
  }

  static datesValidator(control: AbstractControl): {[key: string]: any} | null {
    // function dateTostring(date: Object) {
    //   // alert('hello');
    //   return moment(
    //     new Date(control.value['holidayStart'])
    //   ).format('YYYY-MM-DD');
    // }
    // const startDate = dateTostring(control.value['holidayStart']);
    // const endDate =  dateTostring(control.value['holidayEnd']);
    const startDate = control.value['holidayStart'];
    const endDate =  control.value['holidayEnd'];

    // alert(control.value['holidayEnd'] + '>' + control.value['holidayStart']);
    const cmpDate = startDate > endDate;
    console.log(startDate);
    console.log(endDate);
    console.log(startDate > endDate);
    console.log(control.value);
    // alert('fsdfsd');
    return cmpDate ? {'validDate' : cmpDate} : null ;
  }

}
